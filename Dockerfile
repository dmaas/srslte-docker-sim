FROM ubuntu:bionic

RUN apt-get update && apt-get install -y \
    cmake \
    git-core \
    iproute2 \
    iputils-ping \
    libboost-program-options-dev \
    libconfig++-dev \
    libfftw3-dev \
    libmbedtls-dev \
    libsctp-dev \
    libzmq3-dev \
    && rm -rf /var/lib/apt/lists/*

RUN git clone -b release_20_04_1 https://github.com/srsLTE/srsLTE.git /usr/local/src/srsLTE && \
    mkdir /usr/local/src/srsLTE/build && \
    cd /usr/local/src/srsLTE/build && \
    cmake ../ && \
    make -j$(nproc) && \
    make install && \
    ldconfig

RUN srslte_install_configs.sh service
